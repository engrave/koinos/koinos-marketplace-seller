import {Signer, Contract, Provider, Transaction} from "koilib";
import {marketplaceAbi} from "./abis/marketplace";
import {nftAbi} from "./abis/nft";

const WIF = 'PRIVATE_KEY_HERE'
const FIRST_TOKEN_ID = 51;
const LAST_TOKEN_ID = 100;
const COLLECTION = ''; // Collection contract ID
const PRICE = "5500000000"; // (55.00000000 KOIN)

const MARKETPLACE_CONTRACT = '16tsFkc5RyuwQmXxVABzHHwS513Nu2B9S3';
const KOIN_CONTRACT = '15DJN4a8SgrbGhhGksSBASiSYjGnMU8dGL';

const provider = new Provider(["https://api.koinosblocks.com"]);
const signer = Signer.fromWif(WIF);
signer.provider = provider;

const marketplaceContract = new Contract({
    id: MARKETPLACE_CONTRACT,
    abi: marketplaceAbi,
    provider,
    signer,
});

const nftContract = new Contract({
    id: COLLECTION,
    abi: nftAbi,
    provider,
    signer
});

const mFn = marketplaceContract.functions;
const nFn = nftContract.functions;

(async () => {

    for (let i = FIRST_TOKEN_ID; i <= LAST_TOKEN_ID; i++) {
        const hexEncoded = `0x${Buffer.from(i.toString()).toString("hex")}`;

        const tx = new Transaction({
            signer, provider, options: {
                rcLimit: "500000000"
            }
        });

        await tx.pushOperation(nFn.approve, {
            approver_address: signer.getAddress(),
            to: MARKETPLACE_CONTRACT,
            token_id: hexEncoded
        });

        await tx.pushOperation(mFn.create_order, {
            collection: COLLECTION,
            token_sell: KOIN_CONTRACT, // KOIN
            token_id: hexEncoded,
            token_price: PRICE,
            time_expire: "0"
        });

        const {id} = await tx.send();
        await tx.wait('byTransactionId');
        console.log('Transaction ID: ', id);
    }

})();
